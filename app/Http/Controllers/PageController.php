<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello (){
        // return view('hello')->with('name', 'Veronica');
        $info = array(
          'front-end' => 'Zuitt Coding Bootcamp',
          'topics' => ['HTML', 'CSS', 'JS']
        );
    
        return view('hello')->with($info);
      }
    
      public function index(){
          return view('pages.index');
      }
      public function about(){
          return view('pages.about');
      }
      public function services(){
          return view('pages.services');
      }
}
