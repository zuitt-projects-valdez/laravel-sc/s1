<body>
  <h1>Hello {{$name}} </h1> 
  <!-- double {} because name is inside an object -->

  <!-- accessingthe info array -->
  <!-- NOTICE: didn't call info -->
  @if(count($topics)>0){
    @foreach($topics as $topic)
    <li> {{$topic}} </li>
    @endforeach
  }

  @endif
</body>